//
//  ViewController.m
//  LGScrollLabelDemo
//
//  Created by apple on 15/3/16.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "ViewController.h"
#import "LGScrollLabel.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIButton *startBtn;
@property (strong, nonatomic) IBOutlet UIButton *pauseBtn;

@property (strong, nonatomic) LGScrollLabel * label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _label = [[LGScrollLabel alloc] initWithFrame:CGRectMake(0, 100, 320, 30)];
    [_label setStrings:@[@"oajfijgfjpogjgojaigujoipgjohjep9fijgaprgeogj",@"这是一个字幕循环滚动的demo"]];
    [_label setTextFont:[UIFont fontWithName:@"Marker Felt" size:22.0]];
    [_label setDirection:LGScrollLabelDirectionRTL];
    [_label setSpeed:100];
    
    [_label start];
    
    [self.view addSubview:_label];
    
    [_startBtn addTarget:self action:@selector(processWithStartBtn) forControlEvents:UIControlEventTouchUpInside];
    [_pauseBtn addTarget:self action:@selector(processWithPauseBtn) forControlEvents:UIControlEventTouchUpInside];
}

- (void)processWithStartBtn
{
    [_label resume];
}

- (void)processWithPauseBtn
{
    [_label pause];
}



@end
